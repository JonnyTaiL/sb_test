// Copyright Epic Games, Inc. All Rights Reserved.

#include "SB_TopDownShooterGameMode.h"
#include "SB_TopDownShooterPlayerController.h"
#include "SB_TopDownShooterCharacter.h"
#include "UObject/ConstructorHelpers.h"

ASB_TopDownShooterGameMode::ASB_TopDownShooterGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ASB_TopDownShooterPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("Game/Blueprints/Character/TopDownCharacter.TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
